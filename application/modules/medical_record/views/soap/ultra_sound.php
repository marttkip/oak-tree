<div class="row">
	<div class="col-md-12">
		
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Ultra Sound</h2>
			</header>

			<div class="panel-body">
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>
                <div class="row" style="padding:30px 0 30px 0;">
                    <div class="col-xs-8">
                        <select id='ultrasound_id' name='ultrasound_id' class="form-control custom-select">
                            <option value=''>None - Please Select an Ultrasound</option>
                            <?php echo $ultrasound;?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <button type='submit' class="btn btn-sm btn-success"  onclick="pass_ultrasound(<?php echo $visit_id;?>);"> Add Ultra Sound</button>
                    </div>
                </div>
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>   
            </div>
        </section>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
	
	function close_window(visit_id)
	{
		window.close(this);
	}

	function pass_ultrasound(visit_id)
	{
	  var ultrasound_id = document.getElementById("ultrasound_id").value;
	  save_ultrasound(ultrasound_id, visit_id);
	}
	
	function save_xray(val, visit_id)
	{
		var config_url = $('#config_url').val();
		var nav_link = config_url+"nurse/save_ultrasound/"+val+"/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			get_ultrasound(visit_id);
		});
	}
	
	function get_ultrasound(visit_id)
	{
		var config_url = $('#config_url').val();
		var nav_link = config_url+"nurse/get_ultrasound/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			$("#xray_table", window.opener.document).html(data);
		});
	}
</script>