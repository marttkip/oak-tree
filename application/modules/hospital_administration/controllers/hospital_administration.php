<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hospital_administration extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/database');
		$this->load->model('hr/personnel_model');
		$this->load->model('administration/administration_model');
		$this->load->model('administration/reports_model');
		$this->load->model('companies_model');
		$this->load->model('visit_types_model');
		$this->load->model('departments_model');
		$this->load->model('wards_model');
		$this->load->model('rooms_model');
		$this->load->model('beds_model');
		$this->load->model('services_model');
		$this->load->model('hospital_administration_model');
		$this->load->model('insurance_scheme_model');
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		$this->session->unset_userdata('all_transactions_search');
		
		$data['content'] = $this->load->view('administration/dashboard', '', TRUE);
		
		$data['title'] = 'Dashboard';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function update_service_charges($service_id)
	{
		// get
		// get
		$this->db->where('service_id ='.$service_id.' AND service_charge_status = 1 AND visit_type_id <> 1');
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$service_charge_id = $key->service_charge_id;
				$service_id = $key->service_id;
				$service_charge_name = $key->service_charge_name;
				$service_charge_amount = $key->service_charge_amount;

				$this->db->where('visit_type_id <> 1');
				$query_type = $this->db->get('visit_type');

				if($query_type->num_rows() > 0)
				{
					foreach ($query_type->result() as $key_query) {
						$visit_type_id = $key_query->visit_type_id;

						$this->db->where('service_charge_name = "'.$service_charge_name.'" AND service_id = '.$service_id.' AND service_charge_status = 1 AND visit_type_id = '.$visit_type_id);
						$query_update = $this->db->get('service_charge');

						if($query_update->num_rows() == 0)
						{

							$insert_query = array(
										'service_charge_amount'=>$service_charge_amount,
										'service_id'=>$service_id,
										'service_charge_name'=> $service_charge_name,
										'visit_type_id'=> $visit_type_id,
										'service_charge_status'=>1,
										'created'=>date('Y-m-d H:i:s'),
										'created_by'=>$this->session->userdata('personnel_id'),
										'modified_by'=>$this->session->userdata('personnel_id')
									);
							// var_dump($insert_query); die();
							$this->db->insert('service_charge',$insert_query);

						}


					}
				}
			}
		}
		$this->session->set_userdata("success_message", "Successfully updated charges ");
		redirect('hospital-administration/service-charges/'.$service_idd);
	}

	function import_invoices_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_invoice_template();
	}

	function import_invoices()
	{
		//open the add new product
		$v_data['title'] = 'Import invoices';
		$data['title'] = 'Import invoices';
		$data['content'] = $this->load->view('invoices', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_invoice_import()
	{

		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				// var_dump($_FILES); die();
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_invoices($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Charges';
		$data['title'] = 'Import Charges';
		$data['content'] = $this->load->view('invoices', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	function import_payments_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_payment_template();
	}

	function import_payments()
	{
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_payment_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_payments($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Charges';
		$data['title'] = 'Import Charges';
		$data['content'] = $this->load->view('payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	function import_patients_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_patients_data_template();
	}

	function import_patients_update()
	{
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('patients', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_patients_update_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_patients_update($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Patients';
		$data['title'] = 'Import Patients';
		$data['content'] = $this->load->view('patients', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function update_patient_records()
	{
		$this->db->where('cardnumber >= 2802 AND cardnumber <3000');
		$query_patient = $this->db->get('patients_old');

		if($query_patient->num_rows() > 0)
		{
			foreach ($query_patient->result() as $key => $value) {
				# code...
				$cardnumber = $value->cardnumber;
				$firstname = $value->firstname;
				$middlename = $value->middlename;
				$surname = $value->surname;
				$dateofbirth = $value->dateofbirth;
				$gender = $value->gender;
				$phonenumber = $value->phonenumber;
				$email = $value->email;
				$occupation = $value->occupation;
				$registrationdate = date('Y-m-d',strtotime($value->registrationdate));

				// check if patient number exist

				$this->db->where('patient_number',$cardnumber);
				$query_patient = $this->db->get('patients');

				if($query_patient->num_rows() == 0)
				{

					if($gender == "Male")
					{
						$array['gender_id'] = 1;
					}
					else
					{
						$array['gender_id'] = 2;
					}
					// do an insert 
					$array['patient_id'] = $cardnumber;
					$array['patient_number'] = $cardnumber;
					$array['patient_phone1'] = $phonenumber;
					$array['patient_email'] = $email;
					$array['patient_surname'] = $surname;
					$array['patient_othernames'] = $firstname.' '.$middlename;
					$array['patient_town'] = $occupation;
					$array['patient_date_of_birth'] = $dateofbirth;
					$array['patient_date'] = $registrationdate;
					$array['is_new'] = 1;

					$this->db->insert('patients',$array);

					


				}
				else
				{
					$array_list['is_new'] = 2;
					$this->db->where('patient_number',$cardnumber);
					$this->db->update('patients',$array_list);
				}


			}
			echo "completed";
		}
	}


	public function update_invoices_payments()
	{

		// select * bills  with this card number

		$this->db->where('is_new',1);
		$query_items = $this->db->get('patients');

		if($query_items->num_rows() > 0)
		{
			foreach ($query_items->result() as $key => $value) {
				# code...
				$patient_id = $value->patient_id;

				// select * bills

				$this->db->where('bills.visit_id = patient_visit.id AND patient_visit.cardnumber = '.$patient_id);
				$query_item = $this->db->get('bills,patient_visit');

				if($query_item->num_rows() > 0)
				{
					foreach ($query_item->result() as $keyviews => $value_charged) {
						# code...
						$invoice_number = $value_charged->invoice_number;
						$amount = $value_charged->amount;
						$visit_date = $value_charged->datein;
						$visit_date = date('Y-m-d',strtotime($visit_date));

						// check if the visit exisit

						$this->db->where('visit_id',$invoice_number);
						$query_invoice = $this->db->get('visit');

						if($query_invoice->num_rows() == 0)
						{
							// insert all invoices and all payments 
							$visit_data = array(
												"branch_code" => $this->session->userdata('branch_code'),
												"visit_date" => $visit_date,
												"patient_id" => $patient_id,
												"personnel_id" => $this->session->userdata('personell_id'),
												"insurance_limit" => '',
												"patient_insurance_number" => '',
												"visit_type" => 1,
												"time_start"=>date('H:i:s'),
												"time_end"=>date('H:i:s'),
												"appointment_id"=>0,
												"close_card"=>0,
												"procedure_done"=>'',
												"insurance_description"=>'',
												"invoice_number"=>$invoice_number,
												"visit_id"=>$invoice_number,
												"is_new"=>1,
											);
							if($this->db->insert('visit', $visit_data))
							{
								// insert into the visit charge table

								
								$visit_charge_data = array(
									"visit_id" => $invoice_number,
									"service_charge_id" => 1,
									"created_by" => $this->session->userdata("personnel_id"),
									"date" => $visit_date,
									"visit_charge_amount" => $amount,
									"charged"=>1,
									"visit_charge_delete"=>0,
									"visit_charge_units"=>1,
									"visit_charge_qty"=>1
								);
								if($this->db->insert('visit_charge', $visit_charge_data))
								{

								}
							}
							$this->db->where('invoice_number = '.$invoice_number);
							$array_update['update'] = 1;
							$this->db->update('bills',$array_update);
						}
						else
						{
							$this->db->where('invoice_number = '.$invoice_number);
							$array_update['update'] = 2;
							$this->db->update('bills',$array_update);
						}

						
					}
				}
				

				$array_list2['invoiced'] = 1;
				$this->db->where('patient_id',$patient_id);
				$this->db->update('patients',$array_list2);
			}
		}

		echo "completed";
		
	}

	public function update_patients_payments()
	{		

		// select * bills
		$this->db->where('bills.id = payments_old.bill_id AND payments_old.checked = 0');
		$this->db->select('payments_old.amount AS paid,payments_old.id AS payment_id,bills.*,payments_old.*');
		$query_item = $this->db->get('bills,payments_old');

		if($query_item->num_rows() > 0)
		{
			foreach ($query_item->result() as $key => $value) {
				# code...
				$visit_id = $value->invoice_number;
				$amount_paid = $value->paid;
				$receipt_number = $value->receipt_number;
				$payment_date = date('Y-m-d',strtotime($value->date_paid));
				$payed_by = $value->paid_by;
				$pay_type = $value->pay_type;
				$payment_id = $value->payment_id;

				if($pay_type == 1)
				{
					$payment_method = 1;
				}
				else
				{
					$payment_method = 9;
				}

				$type_payment = 1;
				$transaction_code = $receipt_number;
				$payment_service_id = 3;
				$change = 0;

				// check if the receipt is already in
				$this->db->where('visit_id = '.$visit_id.' AND amount_paid = "'.$amount_paid.'" AND payment_created = "'.$payment_date.'" ');
				$query_amount = $this->db->get('payments');
				if($query_amount->num_rows() == 0)
				{
					$data = array(
						'visit_id' => $visit_id,
						'payment_method_id'=>$payment_method,
						'amount_paid'=>$amount_paid,
						'personnel_id'=>$this->session->userdata("personnel_id"),
						'payment_type'=>$type_payment,
						'transaction_code'=>$transaction_code,
						'payment_service_id'=>$payment_service_id,
						'change'=>$change,
						'payment_created'=>$payment_date,
						'payed_by'=>$payed_by,
						'payment_created_by'=>$this->session->userdata("personnel_id"),
						'approved_by'=>$this->session->userdata("personnel_id"),
						'date_approved'=>$payment_date,
						'is_new'=>1
					);

					$this->db->insert('payments', $data);
				}
				else
				{
					$array_gift['is_new'] = 2;
					$this->db->where('visit_id = '.$visit_id.' AND transaction_code = "'.$transaction_code.'"');
					$this->db->update('payments',$array_gift);
				}	

				$array_gift2['checked'] = 1;
				$this->db->where('id = '.$payment_id.'');
				$this->db->update('payments_old',$array_gift2);		

			}
		}
		echo "success";
	}



	public function delete_department_account($department_account_id,$department_id)
	{
		$array['deleted'] = 1;
		$array['deleted_by'] = $this->session->userdata('personnel_id');

		$this->db->where('department_account_id',$department_account_id);

		if ($this->db->update('department_account',$array))
		{
			$this->session->set_userdata("success_message", "You have successfully deleted the account from department");
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}

		redirect('hospital-administration/department-accounts/'.$department_id);
	}


	public function update_customer_invoices()
	{

		$this->db->where('customer_visit_status  = 0');
		$this->db->order_by('customer_visit_id','ASC');
		$query = $this->db->get('customer_invoices');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$invoice_number = $value->invoice_number;
				$customer_name = $value->customer_name;
				$item_price = $value->item_price;
				$item_name = $value->item_name;
				$customer_id = $value->customer_id;
				$invoice_date = $value->invoice_date;
				$customer_visit_status = $value->customer_visit_status;
				$customer_visit_id = $value->customer_visit_id;
				// var_dump($customer_visit_id); die();
				if($customer_visit_status == 0)
				{
					// update the services and service charge table 
					$service_charge_name = $item_name;
					$charge = $item_price;
					$patient_type = 1;


					// checl first 

					$where_array['service_charge_name'] = $service_charge_name;
					$where_array['service_id'] = 7;

					$this->db->where($where_array);
					$query_charge = $this->db->get('service_charge');


					if($query_charge->num_rows() > 0)
					{

						foreach ($query_charge->result() as $key => $charge_value) {
							# code...
							$service_charge_id = $charge_value->service_charge_id;
						}
						$array_update['service_charge_id'] = $service_charge_id;
						$array_update['customer_visit_status'] = 1;
						$this->db->where('customer_visit_id',$customer_visit_id);
						$this->db->update('customer_invoices',$array_update);

					}
					else
					{
						// var_dump($customer_visit_id); die();
							$visit_data = array(
												'service_id'=>7,
												'service_charge_name'=>$service_charge_name,
												'service_charge_amount'=>$charge,
												'visit_type_id'=>$patient_type,
												'service_charge_status'=>1,
												'created'=>date('Y-m-d H:i:s'),
												'created_by'=>$this->session->userdata('personnel_id'),
												'modified_by'=>$this->session->userdata('personnel_id')
											);
							if($this->db->insert('service_charge', $visit_data))
							{
								$service_charge_id = $this->db->insert_id();

								// update the table with the service_charge id
								$array_update['service_charge_id'] = $service_charge_id;
								$array_update['customer_visit_status'] = 1;
								$this->db->where('customer_visit_id',$customer_visit_id);
								$this->db->update('customer_invoices',$array_update);

								// var_dump($query_charge->num_rows()); die();
							}
							

					}

					
					

				}
				else if($customer_visit_status == 2)
				{
					// lets now update the visit and visit_charge tables right
// var_dump($query); die();


				}

			}
		}
					
	}
	public function update_patient_number_one()
	{
		$this->db->where('patient_id > 0');
		$query = $this->db->get('patients');

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_id = $value->patient_id;
				$patient_surname = $value->patient_surname;
				$patient_othernames = $value->patient_othernames;

				$names = $patient_othernames.' '.$patient_surname;

				
				$patients_array['display_name'] = $names;
				$this->db->where('patient_id',$patient_id);
				$this->db->update('patients',$patients_array);
			}
		}
	}

	// update patients 

	public function update_customer_ids()
	{

		$this->db->where('patient_number_status  = 0');
		$this->db->order_by('patient_number_id','ASC');
		$query = $this->db->get('patients_numbers');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$file_number = $value->file_number;
				$customer_name = $value->customer_name;
				$customer_id = $value->customer_id;
				$patient_number_status = $value->patient_number_status;
				$patient_number_id = $value->patient_number_id;

				// explode id
				$customer_explode = explode(' ', $customer_name);
				$surnames = $customer_explode[0];
				$other_names = $customer_explode[1];
				// $last_name = $customer_explode[2];

				$where = '(patients.display_name LIKE \'%'.mysql_real_escape_string($surnames).'\') AND patient_number = "'.$file_number.'" ';
				$this->db->where($where);
				$query_patients = $this->db->get('patients');
				// var_dump($query_patients);die();
				if($query_patients->num_rows() > 0)
				{
					foreach ($query_patients->result() as $key => $value_patients) {
						# code...
						$patient_id = $value_patients->patient_id;
					}

					$update_array['patient_id'] = $patient_id;
					$update_array['patient_number_status'] = 1;
					$update_array['search_name'] = $surnames;
					$this->db->where('patient_number_id',$patient_number_id);
					$this->db->update('patients_numbers',$update_array);

					$patients_array['current_patient_number'] = $customer_id;
					$this->db->where('patient_id',$patient_id);
					$this->db->update('patients',$patients_array);
				}
			}
		}
	}


	public function update_invoices_with_patienta()
	{

		$this->db->where('patient_number_status  = 2');
		$this->db->order_by('patient_number_id','ASC');
		$query = $this->db->get('patients_numbers');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$file_number = $value->file_number;
				$customer_name = $value->customer_name;
				$customer_id = $value->customer_id;
				$patient_number_status = $value->patient_number_status;
				$patient_number_id = $value->patient_number_id;
				$patient_id = $value->patient_id;

				// explode id
				$customer_explode = explode(' ', $customer_name);
				$surnames = $customer_explode[0];
				$other_names = $customer_explode[1];

				$where = '(customer_name LIKE \'%'.mysql_real_escape_string($other_names).'%\') AND customer_id = "'.$customer_id.'" AND customer_visit_status = 1 ';
				$this->db->where($where);
				$query_patients = $this->db->get('customer_invoices');
				// var_dump($query_patients);die();
				if($query_patients->num_rows() > 0)
				{
					foreach ($query_patients->result() as $key => $value_patients) {
						# code...
						$customer_visit_id = $value_patients->customer_visit_id;

						$update_array['patient_id'] = $patient_id;
						$update_array['customer_visit_status'] = 2;
						$this->db->where('customer_visit_id',$customer_visit_id);
						$this->db->update('customer_invoices',$update_array);
					}
					$patients_array['patient_number_status'] = 3;
					$this->db->where('patient_number_id',$patient_number_id);
					$this->db->update('patients_numbers',$patients_array);
				}
			}
		}
	}
	
	// public fun

	public function update_visits()
	{

		// get patient numbers

		$this->db->where('customer_visit_status = 2');
		$this->db->group_by('invoice_number');
		$query = $this->db->get('customer_invoices');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$invoice_number = $value->invoice_number;
				$customer_name = $value->customer_name;
				$item_price = $value->item_price;
				$item_name = $value->item_name;
				$customer_id = $value->customer_id;
				$invoice_date = $value->invoice_date;
				$patient_id = $value->patient_id;
				$customer_visit_status = $value->customer_visit_status;
				$customer_visit_id = $value->customer_visit_id;



				$visit_data = array(
										"branch_code" => $this->session->userdata('branch_code'),
										"visit_date" => $invoice_date,
										"patient_id" => $patient_id,
										"personnel_id" => $this->session->userdata('personell_id'),
										"insurance_limit" => '',
										"patient_insurance_number" => '',
										"visit_type" => 1,
										"time_start"=>date('H:i:s'),
										"time_end"=>date('H:i:s'),
										"appointment_id"=>0,
										"close_card"=>0,
										"invoice_number"=>$invoice_number
										//"room_id"=>$room_id,
										);
				// var_dump($visit_data); die();
				if($this->db->insert('visit', $visit_data))
				{
					// insert into the visit charge table
					$visit_id = $this->db->insert_id();
					
					$update_array['visit_id'] = $visit_id;
					$update_array['customer_visit_status'] = 3;
					$this->db->where('invoice_number',$invoice_number);
					$this->db->update('customer_invoices',$update_array);
					
				}
			}
		}
	}
	public function update_invoices_amounts()
	{
		$this->db->where('customer_visit_status = 3');
		$query = $this->db->get('customer_invoices');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$invoice_number = $value->invoice_number;
				$customer_name = $value->customer_name;
				$item_price = $value->item_price;
				$item_name = $value->item_name;
				$customer_id = $value->customer_id;
				$invoice_date = $value->invoice_date;
				$patient_id = $value->patient_id;
				$visit_id = $value->visit_id;
				$service_charge_id = $value->service_charge_id;
				$customer_visit_status = $value->customer_visit_status;
				$customer_visit_id = $value->customer_visit_id;
				$quantity = $value->quantity;

				$visit_charge_data = array(
								"visit_id" => $visit_id,
								"service_charge_id" => $service_charge_id,
								"created_by" => $this->session->userdata("personnel_id"),
								"date" => $invoice_date,
								"visit_charge_amount" => $item_price,
								"charged"=>1,
								"visit_charge_delete"=>0,
								"visit_charge_units"=>$quantity,
								"visit_charge_qty"=>$quantity
							);
				// var_dump($visit_charge_data); die();
				if($this->db->insert('visit_charge', $visit_charge_data))
				{

					$visit_charge_id = $this->db->insert_id();	

					$update_array['visit_charge_id'] = $visit_charge_id;
					$update_array['customer_visit_status'] = 4;
					$this->db->where('customer_visit_id',$customer_visit_id);
					$this->db->update('customer_invoices',$update_array);

				}
			}
		}
	}

	public function update_payments()
	{
		$this->db->where('current_payment_status = 0');
		$query = $this->db->get('current_payments');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$invoice_number = $value->invoice_number;
				$amount_paid = $value->amount;
				$payment_date = $value->payment_date;
				$receipt_number = $value->receipt_number;
				$payment_method_id = $value->payment_method_id;
				$current_payment_id = $value->current_payment_id;

				$paid_by = $value->paid_by;
				$paid_to = $value->paid_to;

				$type_payment = 1;
				$transaction_code = $receipt_number;
				$payment_service_id = 16;
				$change = 0;


				$this->db->where('invoice_number = "'.$invoice_number.'"');
				$query_amount = $this->db->get('visit');
				if($query_amount->num_rows() == 1)
				{
					foreach ($query_amount->result() as $key => $value) {
						# code...
						$visit_id = $value->visit_id;
					}
					$data = array(
						'visit_id' => $visit_id,
						'payment_method_id'=>$payment_method_id,
						'amount_paid'=>$amount_paid,
						'personnel_id'=>$this->session->userdata("personnel_id"),
						'payment_type'=>$type_payment,
						'transaction_code'=>$transaction_code,
						'payment_service_id'=>$payment_service_id,
						'change'=>$change,
						'payment_created'=>$payment_date,
						'payed_by'=>$paid_by,
						'payment_created_by'=>$this->session->userdata("personnel_id"),
						'approved_by'=>$this->session->userdata("personnel_id"),
						'date_approved'=>$payment_date,
						'paid_to'=>$paid_to
					);
					// var_dump($data); die();
					if($this->db->insert('payments', $data))
					{
						$payment_id = $this->db->insert_id();
						$update_array['payment_id'] = $payment_id;
						$update_array['current_payment_status'] = 1;
						$this->db->where('current_payment_id',$current_payment_id);
						$this->db->update('current_payments',$update_array);


					}
				}
				
			}
		}
	}
	public function all_unallocated_invoices()
	{
		

		$where = 'customer_visit_status = 1';
		$table = 'customer_invoices';

		$unallocated_invoice_search = $this->session->userdata('unallocated_invoice_search');
		
		if(!empty($unallocated_invoice_search))
		{
			$where .= $unallocated_invoice_search;
		}
		
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/zoho-unallocated-invoices';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->hospital_administration_model->get_all_unaccount_invoices($table, $where, $config["per_page"], $page, $order='invoice_number', $order_method='ASC');
		
				// var_dump($query); die();
		
		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('unallocated_invoices', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function unallocated_invoices()
	{


		$where = 'customer_visit_status = 1';
		$table = 'customer_invoices';

		$invoice_search = $this->session->userdata('invoice_search');
		
		if(!empty($invoice_search))
		{
			$where .= $invoice_search;
			$this->db->where($where);
			$query = $this->db->get('customer_invoices');
		}
		else
		{
			$query = array();
		}
		// var_dump($where); die();

		

		$patient_search = $this->session->userdata('patient_search');
		$where_patient = 'patient_id > 0';
		if(!empty($patient_search))
		{
			$where_patient .= $patient_search;

			$this->db->where($where_patient);
			$patients_query = $this->db->get('patients');
		}
		else
		{
			$patients_query = array();
		}
		
		// var_dump($where_patient); die();

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];		
		$v_data['query'] = $query;
		$v_data['patients_query'] = $patients_query;
		$v_data['page'] = 0;
		$data['content'] = $this->load->view('other_unallocated_invoices', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);



	}

	public function search_unallocated_invoices()
	{
		$invoice_number = $this->input->post('invoice_number');
		
		if(!empty($invoice_number))
		{
			$invoice_number = ' AND invoice_number = "'.$invoice_number.'" ';
		}
		else
		{
			$invoice_number = '';
		}
		
		
		
		$search = $invoice_number;
		$this->session->set_userdata('invoice_search', $search);
		
		redirect('unallocated-invoices');
	}
	public function search_patient_record()
	{
		$patient_number = $this->input->post('patient_number');
		$patient_name = $this->input->post('patient_name');
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patient_number = "'.$patient_number.'" ';
		}
		else
		{
			$patient_number = '';
		}


		if(!empty($_POST['patient_name']))
		{
			$search_title .= ' first name <strong>'.$_POST['patient_name'].'</strong>';
			$surnames = explode(" ",$_POST['patient_name']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		
		
		$search = $patient_number.$surname;
		$this->session->set_userdata('patient_search', $search);
		
		redirect('unallocated-invoices');
	}
	
	public function check_invoice_detail($invoice_number)
	{
		$this->session->unset_userdata('invoice_number');
		$this->session->unset_userdata('patient_search');

		if(!empty($invoice_number))
		{
			$invoice_number = ' AND invoice_number = "'.$invoice_number.'" ';
		}
		else
		{
			$invoice_number = '';
		}
		
		$search = $invoice_number;
		$this->session->set_userdata('invoice_search', $search);
		
		redirect('unallocated-invoices');
	}
	public function allocate_invoices($invoice_number,$patient_id)
	{
		// put patient id on the invoice detail for that patient


		$this->db->where('invoice_number',$invoice_number);
		$this->db->group_by('invoice_number');
		$query = $this->db->get('customer_invoices');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$customer_name = $value->customer_name;

			}

			$update_array['patient_id'] = $patient_id;
			$update_array['customer_visit_status'] = 2;
			$this->db->where('customer_name = "'.$customer_name.'" AND customer_visit_status = 1');
			$this->db->update('customer_invoices',$update_array);

		}
	

		// upadte visits 

		$this->update_visits();
		$this->update_invoices_amounts();
		redirect('accounts/zoho-unallocated-invoices');



	}
	public function search_invoices_customers()
	{

		$customer_name = $this->input->post('customer_name');
		$invoice_number = $this->input->post('invoice_number');
		
		if(!empty($invoice_number))
		{
			$invoice_number = ' AND invoice_number = "'.$invoice_number.'" ';
		}
		else
		{
			$invoice_number = '';
		}

		
		if(!empty($customer_name))
		{
			$customer_name = ' AND customer_name = "'.$customer_name.'" ';
		}
		else
		{
			$customer_name = '';
		}
		
		
		
		$search = $invoice_number.$customer_name;
		$this->session->set_userdata('unallocated_invoice_search', $search);
		
		redirect('accounts/zoho-unallocated-invoices');
	}
	public function close_search()
	{
		$this->session->unset_userdata('unallocated_invoice_search');
		
		redirect('accounts/zoho-unallocated-invoices');
	}

	public function update_new_customers()
	{
		$this->db->where('status = 0');
		$query = $this->db->get('customer_details');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$customer_name = $value->customer_name;
				$customer_detail_id = $value->customer_detail_id;


				$this->db->where('customer_name = "'.$customer_name.'"');
				$query_amount = $this->db->get('customer_invoices');
				if($query_amount->num_rows() > 0)
				{
					foreach ($query_amount->result() as $key => $value) {
						# code...
						$customer_id = $value->customer_id;
					}
					$data = array(
						'customer_id' => $customer_id,
						'status'=>1,
					);
					// var_dump($data); die();
					$this->db->where('customer_detail_id',$customer_detail_id);
					if($this->db->update('customer_details', $data))
					{
					}
				}
				
			}
		}
	}

	public function update_patients_customers()
	{
		$this->db->where('status = 1');
		$query = $this->db->get('customer_details');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$customer_name = $value->customer_name;
				$customer_detail_id = $value->customer_detail_id;
				$file_number = $value->file_number;
				$customer_id = $value->customer_id;

				$data = array(
						'customer_id' => $customer_id,
						'customer_name' => $customer_name,
						'file_number' => $file_number,
						'patient_number_status'=>0,
					);
				// var_dump($data); die();
				if($this->db->insert('patients_numbers', $data))
				{
					$update['status'] = 2;
					$this->db->where('customer_detail_id',$customer_detail_id);
					$this->db->update('customer_details', $update);
				}
				
				
			}
		}
	}
	public function update_non_pharms()
	{
		$this->db->where('product.product_id = service_charge.product_id AND product.product_deleted = 0 AND product.category_id = 3');
		$query = $this->db->get('product,service_charge');
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$service_charge_amount = $value->service_charge_amount;
				$service_charge_id = $value->service_charge_id;


				$array['service_charge_amount'] = $service_charge_amount * 10;
				$this->db->where('service_charge_id',$service_charge_id);
				$this->db->update('service_charge',$array);
			}
		}

	}
}
?>