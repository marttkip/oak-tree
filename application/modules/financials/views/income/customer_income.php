<?php echo $this->load->view('search/income_search','', true);?>

<?php
$all_properties = $this->company_financial_model->get_all_visit_types();
$receivables = '';
$total_invoice = 0;
$total_payments = 0;
$total_balances = 0;
if($all_properties->num_rows() > 0)
{

	foreach ($all_properties->result() as $key => $value) {
		// code...
		$visit_type_id = $value->visit_type_id;
		$visit_type_name = $value->visit_type_name;
		$property_balance = $this->company_financial_model->get_receivable_balances($visit_type_id);

		$invoices = $property_balance['invoice'];
		$payments = $property_balance['payments'];
		$balance = $invoices - $payments;
		$receivables .= '	<tr>
								<td class="text-left">'.strtoupper($visit_type_name).'</td>
								<td class="text-right">'.number_format($invoices,2).'</td>
								<td class="text-right">'.number_format($payments,2).'</td>
								<td class="text-right"><a href="'.site_url().'customer-invoices/'.$visit_type_id.'" >'.number_format($invoices-$payments,2).'</a></td>
							</tr>';
		$total_balances += $balance;
		$total_payments += $payments;
		$total_invoice += $invoices;
	}


}
$receivables .= '	<tr>
						<td class="text-left"><b>Totals</b></td>
						<td class="text-right"><b class="match">'.number_format($total_invoice,2).'</b></td>
						<td class="text-right"><b class="match">'.number_format($total_payments,2).'</b></td>
						<td class="text-right"><b class="match">'.number_format($total_balances,2).'</b></td>
					</tr>';

	$search = $this->session->userdata('customer_income_title_search');
	// var_dump($search);die();
	if(!empty($search))
	{
		$customer_income_search = ucfirst($search);
	}
	else {
		$customer_income_search = 'Reporting as of: '.date('M j, Y', strtotime(date('Y-01-01'))).' to '.date('M j, Y', strtotime(date('Y-m-d')));
	}
?>
<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>

<div class="text-center">
	<h3 class="box-title">Income by Customer</h3>
	<h5 class="box-title"><?php echo $customer_income_search?></h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<section class="panel">
		<header class="panel-heading">
				<h3 class="panel-title">Receivable </h3>
				<a href="<?php echo site_url().'company-financials'?>" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;margin-left: 5px;"> <i class="fa fa-arrow-left"></i> Back to company financials </a>
				<a onclick="javascript:xport.toCSV('testTable');" class="btn btn-sm btn-success pull-right" style="margin-top:-25px;"> Export</a>

		</header>
		<div class="panel-body">
    	<!-- <h3 class="box-title">Revenue</h3> -->
    	<table class="table  table-striped table-condensed" id="testTable">
			<thead>
				<tr>
        			<th class="text-left">Customer</th>
					<th class="text-right">Invoice</th>
					<th class="text-right">Payments</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $receivables?>
			</tbody>
		</table>


    </div>
</section>
<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
