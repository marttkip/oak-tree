<?php echo $this->load->view('search/workload_search', '', TRUE);?>
<?php

$search = $this->session->userdata('workload_report_search');
					
if(!empty($search))
{
	$year_search = $this->session->userdata('workload_year_search');
					
	if(!empty($year_search))
	{
		$year = $year_search;

	}

	$month_search = $this->session->userdata('workload_month_search');
					
	if(!empty($month_search))
	{
		$month = $month_search;

	}

}
else
{
	$year_search = date('Y');
	$month_search = date('m');
	$year = date('Y');
}


// male over 5 years of age

// get the age

$search_year = $year - 5;
$community_where = 'patients.patient_id = visit.patient_id AND patients.gender_id = 1 AND YEAR(patients.patient_date_of_birth) < '.$search_year.' AND  YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'" ';
$community_table = 'visit,patients';
$total_male_over_5 = $this->moh_reports_model->count_items($community_table, $community_where);

// female over 5 years of age

$community_where = 'patients.patient_id = visit.patient_id AND patients.gender_id = 2  AND YEAR(patients.patient_date_of_birth) < '.$search_year.' AND  YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
$community_table = 'visit,patients';
$total_female_over_5 = $this->moh_reports_model->count_items($community_table, $community_where);


// female less 5 years of age

$community_where = 'patients.patient_id = visit.patient_id AND patients.gender_id = 1 AND YEAR(patients.patient_date_of_birth) > '.$search_year.' AND  YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
$community_table = 'visit,patients';
$total_male_less_5 = $this->moh_reports_model->count_items($community_table, $community_where);



$community_where = 'patients.patient_id = visit.patient_id AND patients.gender_id = 2  AND YEAR(patients.patient_date_of_birth) > '.$search_year.' AND  YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
$community_table = 'visit,patients';
$total_female_less_5 = $this->moh_reports_model->count_items($community_table, $community_where);



// new visits
$community_where = 'patients.patient_id = visit.patient_id AND patients.last_visit = "0000-00-00 00:00:00"  AND YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'" ';
$community_table = 'visit,patients';
$new_visits = $this->moh_reports_model->count_items($community_table, $community_where);




// old visits

$community_where = 'patients.patient_id = visit.patient_id AND patients.last_visit != "0000-00-00 00:00:00" AND YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
$community_table = 'visit,patients';
$old_visits = $this->moh_reports_model->count_items($community_table, $community_where);



$community_where = 'visit_type_id > 0  ';
$community_table = 'visit_type';
$visit_types = $this->moh_reports_model->get_all_items($community_table,$community_where);
?>

<div class="row statistics">
   
    
    <div class="col-md-12 col-sm-12">
    	 <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Outpatient Services</h2>

            	<?php
            	$search = $this->session->userdata('workload_report_search');
            	if(!empty($search))
				{
					echo '<a href="'.site_url().'close-workload-search" style="margin-top:-25px;" class="btn btn-warning pull-right btn-sm">Close Search</a>';
				}

            	?>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
						
                        <h5>Patient Age groups</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <tr>
                                    <th>Over 5 - Male</th>
                                    <td><?php echo $total_male_over_5;?></td>
                                </tr>
                                <tr>
                                    <th>Over 5 - Female</th>
                                    <td><?php echo $total_female_over_5;?></td>
                                </tr>
                                <tr>
                                    <th>Children Under 5 - Male</th>
                                    <td><?php echo $total_male_less_5;?></td>
                                </tr>
                                 <tr>
                                    <th>Children Under 5 - Female</th>
                                    <td><?php echo $total_female_less_5;?></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <div class="clearfix"></div>
            		</div>
                    <!-- End Transaction Breakdown -->
                    
                   
                    <div class="col-md-3">
                        <h5>Patient Category</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <tr>
                                    <th>New Visits</th>
                                    <td><?php echo $new_visits;?></td>
                                </tr>
                                <tr>
                                    <th>Re Visits</th>
                                    <td><?php echo number_format($old_visits, 0);?></td>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <td><?php echo number_format($new_visits + $old_visits, 0);?></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <div class="clearfix"></div>
            		</div>

            		<div class="col-md-3">
                        <h5>Patient Visit Type</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                            	<?php
                            	foreach ($visit_types->result() as $key) {
                            		# code...
                            		$visit_type_name = $key->visit_type_name;
                            		$visit_type_id = $key->visit_type_id;

                            		$community_where ='visit.visit_type ='.$visit_type_id.' AND YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
									$community_table = 'visit';
									$total_number = $this->moh_reports_model->count_items($community_table, $community_where);
                            		?>
                            		<tr>
	                                    <th><?php echo $visit_type_name;?></th>
	                                    <td><?php echo number_format($total_number, 0);?></td>
	                                </tr>
                            		<?php

                            	}


                            	?>
                              
                            </tbody>
                        </table>
                        
                        <div class="clearfix"></div>
            		</div>
                    
                    <div class="col-md-3">
                        <h4 class="center-align">Total Patients</h4>
                        <h3 class="center-align"><?php echo number_format($total_male_over_5 + $total_female_over_5 + $total_male_less_5 + $total_female_less_5, 0);?></h3>
                    </div>
                </div>
          	</div>
		</section>
    </div>
</div>

<?php




?>

<div class="row statistics">
   
    
    <div class="col-md-12 col-sm-12">
    	 <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Departments Visits Numbers</h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <?php
					$error = $this->session->userdata('service_error_message');
					$success = $this->session->userdata('service_success_message');
					
					if(!empty($success))
					{
						echo '
							<div class="alert alert-success">'.$success.'</div>
						';
						$this->session->unset_userdata('service_success_message');
					}
					
					if(!empty($error))
					{
						echo '
							<div class="alert alert-danger">'.$error.'</div>
						';
						$this->session->unset_userdata('service_error_message');
					}
					$search = $this->session->userdata('workload_report_search');
					
					
					$result = '';
					
					//if users exist display them
					if ($query->num_rows() > 0)
					{
						$count = $page;
						
						$result .= 
							'
								<table class="table table-hover table-bordered ">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Department Name</th>
									  <th>New Visits</th>
									  <th>Re Visits</th>
									  <th>Total</th>
									</tr>
								  </thead>
								  <tbody>
							';
						
						foreach ($query->result() as $row)
						{
							
							$department_name = $row->department_name;
							$department_id = $row->department_id;
							
							$community_where ='patients.patient_id = visit.patient_id AND patients.last_visit = "0000-00-00 00:00:00" AND visit.visit_id = visit_department.visit_id AND visit_department.department_id  ='.$department_id.' AND YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
							$community_table = 'visit,visit_department,patients';
							$total_number_new = $this->moh_reports_model->count_items($community_table, $community_where);



							$community_where ='patients.patient_id = visit.patient_id AND patients.last_visit <> "0000-00-00 00:00:00" AND visit.visit_id = visit_department.visit_id AND visit_department.department_id  ='.$department_id.' AND YEAR(visit.visit_date) = '.$year_search.' AND MONTH(visit.visit_date) = "'.$month_search.'"';
							$community_table = 'visit,visit_department,patients';
							$total_number_old = $this->moh_reports_model->count_items($community_table, $community_where);
							$total = $total_number_new + $total_number_old;
							$count++;
							$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$department_name.'</td>
										<td>'.$total_number_new.'</td>
										<td>'.$total_number_old.'</td>
										<td>'.$total.'</td>
										
									</tr> 
								';
						}
						
						$result .= 
						'
									  </tbody>
									</table>
						';
					}
					
					else
					{
						$result .= "There are no departments";
					}
					
					echo $result;
					?>
			          </div>
          
	          <div class="widget-foot">
	                                
					<?php if(isset($links)){echo $links;}?>
	            
	                <div class="clearfix"></div> 
	            
	            </div>

              </div>
        </section>
    </div>
</div>