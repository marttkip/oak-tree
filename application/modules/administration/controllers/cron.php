<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('administration/sync_model');
	}
	
	public function sync_visits()
	{
		
		$date = date('Y-m-d');
		//Sync OSE
		$this->session->set_userdata('branch_code', 'OSE');
		$this->db->where('branch_code = "'.$this->session->userdata('branch_code').'" AND visit_date = "'.$date.'"');
		$query = $this->db->get('visit');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$visit_id = $res->visit_id;
				
				if($this->sync_model->syn_up_on_closing_visit($visit_id))
				{
				}
			}
		}
	}
	public function backup()
    {
        // Load the DB utility class
        $this->load->dbutil();
        $date = date('YmdHis');
        $prefs = array(
            'ignore'        => array('table','diseases','icd10'),                     // List of tables to omit from the backup
            'format'        => 'txt',                       // gzip, zip, txt
            'filename'      => $date.'_backup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
            'newline'       => "\n"                         // Newline character used in backup file
        );

        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup($prefs);

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('G://backups/'.$date.'_backup.sql', $backup);

        // Load the download helper and send the file to your desktop
        // $this->load->helper('download');
        // force_download('mybackup.gz', $backup);



    }

      public function update_all_products_table()
    {
    	$this->db->where('service_charge_delete = 0 AND product_id > 0 AND drug_id = 0');
    	$query = $this->db->get('service_charge');

    	if($query->num_rows() > 0)
    	{
    		$updated_charge = 0;
    		foreach ($query->result() as $key => $value) {
    			# code...
    			$product_id = $value->product_id;
    			$service_charge_id = $value->service_charge_id;

    			$array_update['product_id'] = $product_id;
    			$this->db->where('service_charge_id',$service_charge_id);
    			$this->db->update('visit_charge',$array_update);

    			if($updated_charge != $service_charge_id)
    			{
    				$updated_charge = $service_charge_id;
	    			$charge_update['drug_id'] = $product_id;
	    			$this->db->where('product_id',$product_id);
	    			$this->db->update('service_charge',$charge_update);
    			}
    			// var_dump($query); die();

    		}
    	}
    }


	public function update_drugs_charges()
    {
    	$this->db->where('service_charge.service_charge_id = visit_charge.service_charge_id AND visit_charge.product_id > 0  AND visit_charge.buying_price IS NULL');
			$this->db->limit(10000);
    	$query = $this->db->get('service_charge,visit_charge');

    	if($query->num_rows() > 0)
    	{
    		$updated_charge = 0;
    		foreach ($query->result() as $key => $value) {
    			# code...
    			$product_id = $value->product_id;
					$visit_charge_id = $value->visit_charge_id;
    			$service_charge_id = $value->service_charge_id;
					$vatable = $value->vatable;
					$service_charge_amount = $value->service_charge_amount;
					if($vatable > 0)
					{
						$service_charge_amount = ($service_charge_amount *100) /116;
					}

					$service_charge_amount = ($service_charge_amount*100/133);
					// var_dump($service_charge_amount);die();
    			$array_update['buying_price'] = $service_charge_amount;
    			$this->db->where('visit_charge_id',$visit_charge_id);
    			$this->db->update('visit_charge',$array_update);
    		}
    	}
    }
}
?>